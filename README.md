# Prueba básica de Laravel (Aplicación de Fotos)

## Introducción y Herramientas

La prueba consiste en realizar una pequeña aplicación para subir y visualizar fotos. La aplicación debe contar con un login y un registro. Los usuarios registrados/logueados tienen la capacidad de crear albumes y agregar fotos a sus albums. Para ello deben utilizar las siguientes herramientas:

- **Laravel 5.6** ([https://laravel.com/docs/5.6](https://laravel.com/docs/5.6))
- **Bootstrap 4** (incluido por defecto en la instalación de Laravel). Es una librería escrita en CSS que permite crear páginas web responsive y otros componentes de interfaz utilizando clases. La documentación se puede encontrar en [https://getbootstrap.com/docs/4.1/getting-started/introduction/](https://getbootstrap.com/docs/4.1/getting-started/introduction/)
- **Base de datos MySQL o MariaDB**

**Bonus: Utilizar Vue.js** (también incluido por defecto en la instalación de Laravel). Vue.js utiliza la potencia de Javascript para crear aplicaciones con peticiones asíncronas y modificación del DOM sin necesidad de recargar la página mediante una sintaxis declarativa utilizando atributos y tags de HTML, y una lógica aplicada en forma de métodos y hooks del ciclo de vida. La documentación oficial se puede encontrar en [https://vuejs.org/v2/guide/](https://vuejs.org/v2/guide/)

## Requerimientos/Historias de usuario

### Página principal
-	Al ingresar a la página principal pueden suceder 2 cosas, de acuerdo a si el usuario se encuentra logueado o no:
o	Si el usuario no está logueado, se deben ver 2 opciones: iniciar sesión o registrarse.
o	Si el usuario está logueado, debe ser redirigido a su página de usuario.

### Registro

- El registro debe tener 4 campos: Nombre, Correo electrónico, Contraseña y Confirmar contraseña. Los datos deben ser validados, y si existe algún error, debe informársele al usuario. Si todos los datos son correctos, el registro se completa y el usuario es redirigido a su página de usuario.

**Nota:** Las contraseñas almacenadas en base de datos deben ser encriptadas

### Login

- El login debe pedir usuario y contraseña. Si hay un error en los datos, debe informársele al usuario. Si el login es exitoso, el usuario es redirigido a su página de usuario.


### Página de usuario

Esta es la página a la cual llega el usuario después de loguearse/registrarse.

- En esta página se debe ver el nombre del usuario que está logueado.
- Debe haber un enlace/botón para cerrar sesión. Al hacer click en él, se cierra la sesión del usuario y es redirigido a la página principal.
- La página debe mostrar los álbumes que tenga el usuario. Debe darse la opción para editar y eliminar los álbumes.
- Debe haber una opción para crear un nuevo álbum. Al hacer click debe mostrar un formulario con un único campo que es el nombre del álbum. Si ya existe un álbum con el nombre introducido, debe informarsele al usuario e impedir la creación del álbum.
- Al editar un álbum, se muestra un formulario con los mismos campos que el de crear álbum.
- Al eliminar un álbum, también deben eliminarse todas las fotos de dicho álbum.
- Para los formularios de crear y editar, si hay algún error con las entradas, debe informársele al usuario.
- Al crear, editar o eliminar un álbum, debe mostrarse la página principal del usuario con los cambios reflejados.
- Al hacer click en un álbum debe llevar a la página del respectivo álbum

### Página de álbum


Esta página muestra las fotos que tiene dicho álbum

- En la página debe verse el nombre del álbum
- Debe mostrarse una galería con las fotos del álbum. De cada foto debe ser visible una miniatura de la foto y el nombre de esta. Cada foto debe tener una opción para eliminar la foto.
- Debe haber una opción para agregar una nueva foto. Al hacer click debe mostrarse un formulario con un campo de nombre de la foto, otro para una descripción de la foto y una opción para buscar la foto en el computador. Si existe algún error en las entradas, debe informársele al usuario. Si ya existe una foto en el álbum con el mismo nombre introducido en el campo nombre, debe informarsele al usuario e impedir la creación de la foto.
- Al crear o eliminar una foto, debe mostrarse la página del álbum con los cambios reflejados.
- Al hacer click en una de las fotos, debe mostrarse la foto más grande, junto con su nombre y su descripción.

### Requerimiento adicional

La aplicación debe ser responsive (adaptarse al tamaño de la pantalla donde se este visualizando la aplicación). Para esto se utiliza Bootstrap.

## Recomendaciones

- Para la parte de autenticación, se recomienda utilizar el comando `php artisan make:auth`. Esto genera automaticamente las migraciones y las vistas para el login y el registro. No olvidar correr el comando `php artisan migrate` para crear las tablas especificadas en las nuevas migraciones. Más información se puede encontrar en [https://laravel.com/docs/5.6/authentication](https://laravel.com/docs/5.6/authentication).
- Laravel utiliza SCSS por defecto para generar los estilos de la aplicación. SCSS es una extensión de CSS, lo que quiere decir que acepta la sintaxis de CSS pero además da acceso a poderosas herramientas como variables, estilos anidados, mixins, entre otras. En el directorio resources/assets/sass se encuentran los archivos de estilos incluidos con laravel. Se recomienda agregar los estilos en el archivo app.scss. La documentación de SCSS se puede encontrar en [https://sass-lang.com/guide](https://sass-lang.com/guide), donde se encuentra información de como utilizar las poderosas herramientas mencionadas anteriormente. Sin embargo, también es posible escribir únicamente CSS puro en los archivos SCSS.
- De manera similar a los estilos, Laravel trae por defecto Vue.js, pero además otras librerias de gran utilidad como Lodash, JQuery y Axios. En el directorio resources/assets/js hay una carpeta components con un componente de Vue como ejemplo, y dos archivos bootstrap.js y app.js. Estos se encargan de cargar las librerias ya mencionadas y los componentes de Vue declarados.
- La calidad del código es muy importante. Intente aprovechar la máximo las convenciones y herramientas que proporciona Laravel.

#### Compilación de SCSS y Javascript

Tanto para los estilos en app.scss como para el Javascript en app.js, es necesario compilar estos archivos, tras lo cual se generan 2 archivos en el directorio public, css/app.css y js/app.js. Estos archivos contienen todos los estilos y código javascript respectivamente. Para compilar estos archivos, primero se deben instalar las dependencias de javascript.

Para instalar las dependencias se debe tener instalado Node.js, el cual contiene npm, el manejador de paquetes de Javascript. Puede descargar Node.js de su página oficial [https://nodejs.org/es/](https://nodejs.org/es/).

Cuando Node.js instalado, npm queda instalado y listo para usar desde el terminal. **Importante:** Si recien se instala Node.js, deben cerrarse todas las ventanas del terminal que esten abiertas, ya que en estas no se verán reflejados los cambios, y por tanto no estará disponible el comando npm.

Una vez instalado Node.js, desde el terminal se navega hasta la carpeta donde está el proyecto. Una vez allí, se corre el comando `npm install`. Una vez terminé de ejecutarse este comando se pueden compilar los archivos. Existen varios comandos para compilar los archivo, pero los 3 más básicas son:

1. `npm run dev`: Compila los archivos sin minificar
2. `npm run watch`: Compila los archivos y continua ejecutandose, observando los cambios en los archivos. Al detectar cambios, re compila los archivos.
3. `npm run prod`: Compila los archivos y los minifica.

Se recomienda utilizar `npm run watch` y dejarlo corriendo en una ventana del terminal. De esta forma se podrán ver los cambios reflejados en la aplicación al guardar los archivos y recargar la página. Más información acerca de SCSS y Vue con Laravel se puede encontrar en [este enlace](https://laravel.com/docs/5.6/frontend) e información acerca de la compilación de estos archivos [aquí](https://laravel.com/docs/5.6/mix).

